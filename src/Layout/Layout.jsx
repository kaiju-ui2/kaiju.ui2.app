import React from "react";
import {MainMenu} from "Nav";

import {inject} from "mobx-react";
import ContentBody from "Pages/ContentBody/ContentBody";
/** Разметка страницы . */

@inject("routerStore")
export default class MainMenuLayout extends React.Component {

    render() {
        return (
            <React.Fragment>
                <div className="page">
                <MainMenu
                    navigateToProfile={() => this.props.navigateToProfile()}
                    conf={this.props.loadMainMenuConf()}/>
                <ContentBody routerStore={this.props.routerStore}/>
                </div>
            </React.Fragment>
        )
    }
}