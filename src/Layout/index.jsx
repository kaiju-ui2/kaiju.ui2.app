import MainMenuLayout  from './Layout';
import PageContentComponent, {NoSidebarPageContent} from "Layout/PageContentComponent";


export default MainMenuLayout;
export {
    PageContentComponent,
    NoSidebarPageContent
}