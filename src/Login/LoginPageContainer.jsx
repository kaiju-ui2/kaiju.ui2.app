import React from "react";
import {inject} from "mobx-react";
import {routeNode} from "react-mobx-router5";
import "./LoginPageContainer.scss"
import LoginForm from "./LoginForm";

const routeNodeName = 'login';


@inject("routerStore")
class LoginPageContainer extends React.Component {

    componentWillUnmount() {
        if (document.querySelector('.page')) {
            document.querySelector('.page').classList.remove('light');
        }
    }

    render() {
        if (document.querySelector('.page')) {
            document.querySelector('.page').classList.add('light');
        }
        let motto = this.props.routerStore.projectMotto();
        let projectLabel = this.props.routerStore.projectLabel();
        let projectLogo = this.props.routerStore.projectLogo();

        return (
            <div className="auth">
                <div id="m_login">
                        <div className="">
                            <div className="text-center auth__head" style={{fontSize: "28px"}}>
                                {projectLogo && (<img src={projectLogo} />)}
                                [{projectLabel && projectLabel}]
                                <div className="mt-4">
                                    {motto && <h3 className="auth__title">{motto}</h3>}
                                </div>
                            </div>
                            <LoginForm/>
                        </div>
                </div>
            </div>
        )
    }
}


export default routeNode(routeNodeName)(LoginPageContainer);
