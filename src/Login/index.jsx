import LoginForm from "./LoginForm";
import LoginPageContainer from "./LoginPageContainer";

const loginRoute = {
    name: "login",
    path: "/login",
    component: LoginPageContainer,
};


export {
    LoginForm,
    LoginPageContainer,
    loginRoute
}