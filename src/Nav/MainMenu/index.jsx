import React from "react";
import {inject} from 'mobx-react';
import "Nav/MainMenu/MainMenu.scss"
import ReactTooltip from "react-tooltip";
import UserMenuLink from "./UserMenuLink";
import {withRoute} from "react-mobx-router5";


class Link extends React.Component {

    render() {
        const id = utils.uuid4();
        return (
            <li className={`main-menu__item ${this.props.isActive ? 'main-menu__item--active' : ''}`}>
                <a href={this.props.href}
                   onClick={(e) => {
                       e.preventDefault();
                       this.props.onClick()
                   }}
                   className="main-menu__link" data-for={id} data-tip>
                    {this.props.icon}
                </a>

                <ReactTooltip
                    id={id}
                    aria-haspopup={true}
                    className="main-menu__tooltip main-menu__tooltip_link"
                    scrollHide={true}
                    place="right"
                    effect="solid">

                    <div>{this.props.label}</div>
                </ReactTooltip>
            </li>
        );
    }
}

const LinkWithRoute = withRoute(Link);

@inject('routerStore')
@inject('userStore')
class MainMenu extends React.Component {

    render() {
        let {userStore, routerStore, conf, routes} = this.props;

        if (!userStore.isLoggedIn) {
            return <React.Fragment/>
        }

        routes = (conf || routes).filter(route => userStore.hasAnyPerm(route.permission));

        return (
            <React.Fragment>
                <ul className="main-menu">
                            <UserMenuLink key="user" navigateToProfile={() => this.props.navigateToProfile()}/>
                            {routes.map((route) => {
                                return (
                                    <LinkWithRoute
                                        key={utils.uuid4()}
                                        routeName={route.rootName}
                                        href={routerStore.router.buildPath(route.name)}
                                        activeStrict={false}
                                        onClick={() => routerStore.router.navigate(route.name)}
                                        {...route}
                                    />
                                )
                            })}
                </ul>
            </React.Fragment>

        );
    }
}

export default withRoute(MainMenu)