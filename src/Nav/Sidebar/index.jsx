import React from 'react'
import {inject, observer} from "mobx-react";
import {withRoute} from "react-mobx-router5";
import { Resizable } from "re-resizable";

class ListElement extends React.Component {
    render() {
        const {isActive, onClick, label, icon} = this.props;
        return (
            <div className="pb-3 pl-4 font-weight-normal" key={utils.uuid4()}>
                <a
                    onClick={() => onClick()}
                    style={{"textDecoration": "none", cursor: "pointer", display: "table"}}
                    className={isActive ? "active" : "text-body"}>
                    {icon &&
                    <span className={`${icon} pr-3 fs-22`}/>
                    }
                    <span style={{display: "table-cell", verticalAlign: "top"}}>{label}</span>
                </a>
            </div>
        )
    }
}

const WithRouteListElement = withRoute(ListElement);

class SidebarContainer extends React.Component {
    render() {
        return (
            <Resizable className="product-sidebar border-right"
            key={utils.uuid4()}
            style={{
                'overflowY': "auto",
                'overflowX': "hidden",
                "backgroundColor": "white",
                paddingBottom: "30px",
                zIndex: 1
            }}
            defaultSize={{
                width: localStorage.getItem('sidebarWidth') ? localStorage.getItem('sidebarWidth') : 250
            }}
            onResizeStop={(event, direction, ref)=>localStorage.setItem('sidebarWidth', ref.offsetWidth)}
            maxWidth={450}
            minWidth={200}
            enable={{
                top: false,
                right: true,
                bottom: false,
                left: false,
                topRight: false,
                bottomRight: false,
                bottomLeft: false,
                topLeft: false,
            }}>
           {this.props.children}
       </Resizable>
        )
    }
}

/*
With permissions
 */
@inject('routerStore')
@inject('userStore')
@observer
class SidebarList extends React.Component {

    renderNavElement({routes, label}, i) {
        let {activeStrict = false, params = {}, userStore} = this.props;

        routes = routes.filter(route => userStore.hasAnyPerm(route.permission));

        return (
            <React.Fragment key={utils.uuid4()}>
                <div
                    className={`pt-3 pb-3 pl-4 pr-4 text-black-50 ${i !== 0 ? "border-top" : ""}`}>{label}</div>
                <div className="pb-3 pt-3">
                    {
                        routes.map((route) => {
                            return <WithRouteListElement
                                key={route.name}
                                icon={route.icon}
                                onClick={() => this.props.routerStore.navigate(route.name, params)}
                                routeName={route.rootName || route.name}
                                routeParams={params}
                                activeStrict={activeStrict}
                                label={route.label}
                            />
                        })}
                </div>
            </React.Fragment>
        )
    }


    render() {
        return (
            <React.Fragment key={utils.uuid4()}>
                {this.props.conf.map((menu, i) => this.renderNavElement(menu, i))}
            </React.Fragment>
        )
    }
}

/*
With permissions
 */
class Sidebar extends React.Component {
    render() {
        return (
            <SidebarContainer>
                <SidebarList conf={this.props.conf} params={this.props.params}/>
            </SidebarContainer>
        )
    }
}

export {SidebarList, SidebarContainer, Sidebar}