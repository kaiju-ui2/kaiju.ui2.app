/** Корневая нода, из которой исходят все деревья. */

import React from "react";
import PropTypes from 'prop-types';
import {routeNode, RouteView} from "react-mobx-router5";

const routeNodeName = ''; // '' because root node

class ContentBody extends React.Component {
  render() {
    const {route, routerStore} = this.props;
    // This will inject 'route' to the selected child component
    return <RouteView route={route} routes={routerStore.routesMap} routeNodeName={routeNodeName} />;
  }
}

// All injected by routeNode
ContentBody.propTypes = {
  route: PropTypes.object, // observable
  plainRoute: PropTypes.object, // non-observable plain js obj
  routerStore: PropTypes.object
};

// HOC to wrap a route node components
export default routeNode(routeNodeName)(ContentBody);
