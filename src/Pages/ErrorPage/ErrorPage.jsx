import React from "react";
import "./ErrorPage.scss"

/**
 * Компонент для отображения ошибки
 */
export default class ErrorPage extends React.Component {

    getErrorText() {

        switch (this.props.errorCode) {
            case 404:
                return utils.getTranslation(`Message.404`);

            case 403:
                return utils.getTranslation(`Message.403`);

            default:
                return utils.getTranslation(`Message.500`)
        }

    }

    render() {
        return (

            <div className="m-portlet__body ml-auto mr-auto error-page">
                <div className="row">
                    <div className="error-page-img">
                        <h1>{this.getErrorText()}</h1>
                    </div>
                </div>
            </div>
        )
    }
}