import React from "react";
import {inject, observer} from "mobx-react";
import {NoSidebarPageContent} from "Layout";

@inject('routerStore')
@observer
export default class ExamplePage extends React.Component {

    render() {
        return (
            <NoSidebarPageContent>
                <div className="p-5" key={utils.uuid4()}>
                    {`Name: ${this.props.route.name}`}<br/>
                    {`Params: ${JSON.stringify(this.props.route.params || {})}`}<br/>
                    {`Fullpath: ${this.props.route.path}`}
                </div>
            </NoSidebarPageContent>
        )
    }
}
