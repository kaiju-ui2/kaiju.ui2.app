import React from "react";
import DynamicHeader from "@kaiju.ui2/components/src/Header/DynamicHeader";
import {Tab, TabList, TabPanel, Tabs} from "@kaiju.ui2/components/src//Tabs";
import Form from "@kaiju.ui2/forms/src/Form";
import {inject, observer, Provider} from "mobx-react";

import asyncErrorDecorator from "router/asyncErrorComponent";
import EditFormStore from "@kaiju.ui2/forms/src/Form/EditFormStore";
import TableComponent from "@kaiju.ui2/components/src/Tables/TableComponent";
import AddCheckboxSelectComponent from "@kaiju.ui2/components/src/Select/AddCheckboxSelectComponent";
import {TableStore} from "@kaiju.ui2/components/src/Tables/stores";
import axios from "axios";
import {toJS} from "mobx";
import ActionsComponent, {DeleteAction} from "@kaiju.ui2/components/src/Tables/actions";
import PermissionsComponent from "./PermissionsComponent";
import PermissionsStore from "./permissionsStore";
import BaseStorage from "@kaiju.ui2/components/src/stores/storage";
import ROUTES  from "System/systemRoutesNames";

const getTableActionComp = (groupId) => {
    return class TableActionsComponent extends React.Component {
        constructor(props) {
            super(props)
        }

        delete() {
            axios.post(ROUTES.rpc,
                {
                    "method": "users_gui.modify_group_users",
                    "params": {
                        id: groupId,
                        user_id: this.props.rowStore.id,
                    }
                }
            ).then(response => {
                if (response.data.result) {
                    this.props.tableStore.actions.fetchByPage()

                } else {
                    utils.handleNotifyExceptions(response)
                }
            }).catch(response => {
                this.isFetching = false;
                utils.handleNotifyExceptions(response)
            })
        }

        getLabel() {
            let {word = {}} = toJS(this.props.rowStore.row);
            return word.value
        }

        render() {
            return (
                <ActionsComponent>
                    <DeleteAction
                        idLabel={this.getLabel()}
                        actionLabel={utils.getTranslation('user_groups.remove_users')}
                        deleteCallback={() => this.delete()}/>
                </ActionsComponent>
            )
        }
    }
};


@asyncErrorDecorator()
@inject("asyncErrorStore")
@inject("routerStore")
@inject("editStore")
@inject("userStore")
@observer
export default class GroupsEditPage extends React.Component {

    constructor(props) {
        super(props);

        this.id = this.props.route.params["id"];

        this.permissionFormStore = new EditFormStore({
                formStore: {
                    disableGroup: true,
                    key: utils.uuid4(),
                    prefix: "groups.groups_form",
                    hasHeader: false,
                    requests: {
                        load: {
                            method: "groups_gui.get",
                            params: {
                                id: this.id
                            },
                            errorCallback: (responseData) => {
                                let errorCode = responseData.error ? responseData.error.code : '';
                                this.props.asyncErrorStore.setError(errorCode)
                            },
                        }
                    }
                },
                requests: {
                    save: {
                        method: "groups_gui.update_info",
                        params: {
                            id: this.id,
                        }
                    }
                },
                changesCallback: (isChanged) => {
                    this.props.editStore.setChanged(isChanged, "grps")
                }
            }
        );

        this.permissionStore = new PermissionsStore({
                requests: {
                    load: {
                        method: "groups_gui.load_permissions",
                        params: {
                            "id": this.id
                        }
                    },
                    save: {
                        method: "groups_gui.update_permissions",
                        params: {
                            "id": this.id,
                            "permissions": {},
                        }
                    }
                },
                changesCallback: (isChanged) => {
                    this.props.editStore.setChanged(isChanged, "permissions")
                }
            }
        );

        this.tableStore = new TableStore({
            prefix: "UsersGrid",
            request: {
                method: "users_gui.grid",
                params: {
                    group: this.id
                }
            },
            rowCallback: (rowStore) => {
                let params = {idOrUsername: rowStore.id};
                let path = this.props.routerStore.router.buildPath(ROUTES.userEdit, params);
                window.open(path, "_blank")
            },
            ActionsComponent: getTableActionComp(this.id)
        });


        this.buttonConf = {
            label: utils.getTranslation("Button.save"),
            icon: "icon-plus",
            onClick: () => {
                if (this.permissionFormStore && this.permissionFormStore.isChanged) {
                    this.permissionFormStore.saveCallback()
                }

                if (this.permissionStore && this.permissionStore.actions.isChanged) {
                    this.permissionStore.actions.save()
                }
            }
        };

        this.choiceUsers = {
            conf: {
                options_handler: "users_gui.load",
                label: "Добавить"
            },
            onChange: (ids) => this.addUsers(ids)
        }

    };

    addUsers(ids) {
        axios.post(ROUTES.rpc,
            {
                "method": "users_gui.modify_group_users",
                "params": {
                    "id": this.id,
                    "user_id": ids
                }
            }
        ).then(response => {
            if (response.data.result) {
                this.tableStore.actions.fetchByPage(1)

            } else {
                utils.handleNotifyExceptions(response)
            }
        }).catch(response => {
            this.isFetching = false;
            utils.handleNotifyExceptions(response)
        })
    }

    DynamicHeader = observer(() => {
        let breadcrumbs = [
            {
                label: 'Группы',
                path: ROUTES.groups
            },
            {
                label: 'Редактирование',
            },
        ];

        return (
            <DynamicHeader
                breadcrumbs={breadcrumbs}
                buttonConf={this.buttonConf}
                isChanged={this.props.editStore.isChanged}
                isFetching={this.permissionFormStore.isFetching}
                label={this.id}
            />
        )
    });

    render() {

        return (
            <React.Fragment>
                <this.DynamicHeader/>
                <Tabs defaultIndex={BaseStorage.getItem("system_groups", 0)}
                      onSelect={(index) => BaseStorage.setItem("system_groups", index)}
                      className="page-content-body pl-5 pr-5">
                    <TabList>
                        <Tab>{utils.getTranslation("Group.general")}</Tab>
                        <Tab>{"Права доступа"}</Tab>
                        <Tab>{"Пользователи"}</Tab>
                    </TabList>
                    <TabPanel>
                        {
                            this.permissionFormStore.dataInitialized &&
                            <div className={this.permissionFormStore.isFetching ? "disabled-form mt-5" : " mt-5"}>
                                <Form key={this.permissionFormStore.formStore.key}
                                      store={this.permissionFormStore.formStore}
                                      groupFiltering/>
                            </div>
                        }
                    </TabPanel>
                    <TabPanel>
                        <PermissionsComponent store={this.permissionStore}/>
                    </TabPanel>
                    <TabPanel>
                        <div className="table-dropdowns">
                            <div>
                                <AddCheckboxSelectComponent
                                    dropdownClassName="right-dropdown"
                                    {...this.choiceUsers}/>
                            </div>
                        </div>
                        <Provider tableStore={this.tableStore}>
                            <TableComponent
                                height={500}
                                className="pb-5"
                                key={utils.uuid4()}/>
                        </Provider>
                    </TabPanel>
                </Tabs>
            </React.Fragment>
        )
    }
}