import {action, computed, observable} from "mobx";
import axios from "axios";
import ROUTES from "System/systemRoutesNames";
import {handleNotifyExceptions} from "@kaiju.ui2/components/src/utils/exceptions";

class PermissionGroupStore {
    @observable permissions;

    constructor(store, data) {
        this.mainStore = store;
        this.id = data.tag;
        this.permissions = data.permissions;
    }

    @computed get isAllChecked() {
        return this.permissions.filter(el => el.value === false).length === 0;
    }

    @computed get isAnyChecked() {
        let truePermissions = this.permissions.filter(el => el.value === true);

        return (
            truePermissions.length > 0 && truePermissions.length !== this.permissions.length
        )
    }

    showGroup() {
        this.mainStore.actions.showGroup(this)
    }


    @action
    updateAllPermissions(value) {
        this.showGroup();

        this.permissions.map((permissionInfo, index) => {

                this.permissions[index].value = value;

                this.mainStore.actions.updatePermission(permissionInfo.id, value)

            }
        )
    }

    @action
    updatePermission(index, value) {
        this.permissions[index].value = value;
        this.mainStore.actions.updatePermission(this.permissions[index].id, value)
    }

}


class PermissionActions {
    @observable query;
    @observable isFetching = false;
    @observable toUpdate = {};

    constructor(store) {
        this.mainStore = store;
        this.fetch()
    }

    @action
    initData(permissions) {
        this.mainStore.permissions = permissions.map(
            data => new PermissionGroupStore(this.mainStore, data)
        );
        this.showGroup(this.mainStore.permissions[0]);
    }

    @action
    showGroup(group) {
        this.mainStore.currentGroup = group
    }

    @action
    updatePermission(permission, value) {
        // если есть в списке - просто убирает его, не добавляя
        if (this.toUpdate[permission] === undefined) {
            this.toUpdate[permission] = value
        } else {
            delete this.toUpdate[permission]
        }

        if (this.mainStore.conf.changesCallback) {
            this.mainStore.conf.changesCallback(this.isChanged)
        }
    }

    @computed get isChanged() {
        return Object.keys(this.toUpdate).length > 0;
    }

    @action
    setQuery(query) {

        if (!query) {
            this.query = undefined;
            return
        }
        this.query = query.toLowerCase();
    }


    @action
    startFetch() {
        this.isFetching = true;
    }

    @action
    stopFetch() {
        this.isFetching = false
    }

    @computed get filteredPermissions() {
        if (!this.query) {
            return this.mainStore.permissions;

        }
        return this.mainStore.permissions.filter(el => {
            this.query.includes(el.id.toLowerCase());
            return el.id.toLowerCase().includes(this.query);

        })
    }

    @action
    fetch() {
        this.startFetch();
        let params = this.mainStore.conf.requests.load.params;
        axios.post(ROUTES.rpc,
            {
                "method": this.mainStore.conf.requests.load.method,
                "params": {
                    ...params,
                }
            }
        ).then(response => {
            if (response.data.result) {
                let permissions = response.data.result.data;

                this.initData(permissions);

            }
            this.stopFetch();
        }).catch(response => {
            this.stopFetch();
            handleNotifyExceptions(response)
        })
    };

    @action
    clearToUpdate() {
        this.toUpdate = {};

        if (this.mainStore.conf.changesCallback) {
            this.mainStore.conf.changesCallback(this.isChanged)
        }
    }

    @action
    save() {
        if (!this.isChanged) {
            return
        }

        this.startFetch();
        let params = this.mainStore.conf.requests.save.params;

        params["permissions"] = this.toUpdate;

        axios.post(ROUTES.rpc,
            {
                "method": this.mainStore.conf.requests.save.method,
                "params": {
                    ...params,
                }
            }
        ).then(response => {
            if (response.data.result) {
                this.clearToUpdate()
            }
            this.stopFetch();
        }).catch(response => {
            this.stopFetch();
            handleNotifyExceptions(response)
        })
    }
}

class PermissionsStore {
    @observable permissions = [];
    @observable currentGroup;

    groups = [];

    constructor(conf) {
        this.conf = conf;

        this.actions = new PermissionActions(this)
    }


}


export default PermissionsStore;