import React from "react";
import Header from "@kaiju.ui2/components/src/Header/base";
import TableComponent from "@kaiju.ui2/components/src/Tables/TableComponent";
import {inject, observer, Provider} from "mobx-react";
import {TableStore} from "@kaiju.ui2/components/src/Tables/stores";
import ROUTES from "System/systemRoutesNames";
import {TransparentWindowFormComponent, WindowFormStore} from "@kaiju.ui2/forms/src/WindowForm";

@inject("routerStore")
export default class GroupsPage extends React.Component {

    constructor(props) {
        super(props);

        this.tableStore = new TableStore({
            prefix: "groups_gui",
            request: {
                method: "groups_gui.grid",
            },
            rowCallback: (rowStore) => {
                this.props.routerStore.router.navigate(ROUTES.groupEdit, {id: rowStore.id})
            }
        });

        this.windowCreateFormStore = new WindowFormStore({
            label: 'Создать группу',
            formStore: {
                key: utils.uuid4(),
                prefix: "GroupsCreate",
                disableGroup: true,
                hasHeader: false,
                requests: {
                    load: {
                        method: "groups_gui.create_model",
                        params: {}
                    }
                }
            },
            requests: {
                save: {
                    method: "groups_gui.create",
                    params: {}
                }
            },
            successCallback: (data) => {
                this.props.routerStore.navigate(ROUTES.groupEdit, {id: data.id})
            }
        });


        this.buttonConf = {
            label: utils.getTranslation("Button.create"),
            icon: "icon-plus",
            onClick: () => {
                this.windowCreateFormStore.toggleWindow();
            }
        };
    }

    DynamicHeader = observer(() => {
        let breadcrumbs = [
            {
                label: 'Система'
            },
            {
                label: 'Группы',
            },
        ];

        return (
            <Header
                buttonConf={this.buttonConf}
                breadcrumbs={breadcrumbs}
                searchCallback={(query) => this.tableStore.actions.fetchByQuery(query)}
            />
        )
    });

    render() {
        return (
            <React.Fragment>
                <Provider tableStore={this.tableStore} windowFormStore={this.windowCreateFormStore}>
                    <TransparentWindowFormComponent/>
                    <this.DynamicHeader/>
                    <div id="table">
                        <TableComponent paginationClassName="pl-5 pr-5" className="pb-5 pl-5 pr-5" key={utils.uuid4()}/>
                    </div>
                </Provider>
            </React.Fragment>
        )
    }
}

