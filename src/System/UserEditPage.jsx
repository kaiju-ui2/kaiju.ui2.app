import React from "react";
import DynamicHeader from "@kaiju.ui2/components/src/Header/DynamicHeader";
import {Tab, TabList, TabPanel, Tabs} from "@kaiju.ui2/components/src//Tabs";
import Form from "@kaiju.ui2/forms/src/Form";
import {inject, observer} from "mobx-react";

import asyncErrorDecorator from "router/asyncErrorComponent";
import EditFormStore from "@kaiju.ui2/forms/src/Form/EditFormStore";
import ROUTES from "System/systemRoutesNames";
import uuid4 from "@kaiju.ui2/components/src/utils";

@asyncErrorDecorator()
@inject("asyncErrorStore")
@inject("routerStore")
@inject("editStore")
@inject("userStore")
@observer
export default class UserEditPage extends React.Component {

    constructor(props) {
        super(props);

        this.idOrUsername = this.props.route.params["idOrUsername"];

        this.formHandleStore = new EditFormStore({
                labelKey: "username",
                formStore: {
                    disableGroup: true,
                    key: utils.uuid4(),
                    prefix: "users_gui.form",
                    hasHeader: false,
                    requests: {
                        load: {
                            method: "users_gui.get",
                            params: {
                                id: this.idOrUsername
                            },
                            errorCallback: (responseData) => {
                                let errorCode = responseData.error ? responseData.error.code : '';
                                this.props.asyncErrorStore.setError(errorCode)
                            },
                        }
                    }
                },
                requests: {
                    save: {
                        method: "users_gui.update_info",
                        params: {
                            id: this.idOrUsername,
                        }
                    }
                },
                changesCallback: (isChanged) => {
                    this.props.editStore.setChanged(isChanged, "usr")
                }
            }
        );

        this.passwordFormStore = new EditFormStore({
                formStore: {
                    disableGroup: true,
                    key: utils.uuid4(),
                    prefix: "users_gui.password_form",
                    hasHeader: false,
                    requests: {
                        load: {
                            method: "users_gui.password_model",
                            params: {},
                            errorCallback: (responseData) => {
                                let errorCode = responseData.error ? responseData.error.code : '';
                                this.props.asyncErrorStore.setError(errorCode)
                            },
                        }
                    }
                },
                requests: {
                    save: {
                        method: "users_gui.change_password",
                        params: {
                            id: this.idOrUsername,
                        }
                    }
                },
                changesCallback: (isChanged) => {
                    this.props.editStore.setChanged(isChanged, "pswd")
                }
            }
        );

        this.groupsFormStore = this.props.userStore.hasPerm("system") ? new EditFormStore({
                formStore: {
                    disableGroup: true,
                    key: utils.uuid4(),
                    prefix: "users_gui.groups_form",
                    hasHeader: false,
                    requests: {
                        load: {
                            method: "users_gui.groups_model",
                            params: {
                                id: this.idOrUsername
                            },
                            errorCallback: (responseData) => {
                                let errorCode = responseData.error ? responseData.error.code : '';
                                this.props.asyncErrorStore.setError(errorCode)
                            },
                        }
                    }
                },
                requests: {
                    save: {
                        method: "users_gui.update_groups",
                        params: {
                            id: this.idOrUsername,
                        }
                    }
                },
                changesCallback: (isChanged) => {
                    this.props.editStore.setChanged(isChanged, "grps")
                }
            }
        ) : {};


        this.statusFormStore = this.props.userStore.hasPerm("system") ? new EditFormStore({
                formStore: {
                    disableGroup: true,
                    key: utils.uuid4(),
                    prefix: "users_gui.status_form",
                    hasHeader: false,
                    requests: {
                        load: {
                            method: "users_gui.status_model",
                            params: {
                                id: this.idOrUsername
                            },
                            errorCallback: (responseData) => {
                                let errorCode = responseData.error ? responseData.error.code : '';
                                this.props.asyncErrorStore.setError(errorCode)
                            },
                        }
                    }
                },
                requests: {
                    save: {
                        method: "users_gui.update_status",
                        params: {
                            id: this.idOrUsername,
                        }
                    }
                },
                changesCallback: (isChanged) => {
                    this.props.editStore.setChanged(isChanged, "status")
                }
            }
        ) : {};

        this.buttonConf = {
            label: utils.getTranslation("Button.save"),
            icon: "icon-plus",
            onClick: () => {
                if (this.passwordFormStore.isChanged) {
                    this.passwordFormStore.saveCallback()
                }

                if (this.groupsFormStore && this.groupsFormStore.isChanged) {
                    this.groupsFormStore.saveCallback()
                }

                if (this.statusFormStore && this.statusFormStore.isChanged) {
                    this.statusFormStore.saveCallback()
                }

                if (this.formHandleStore && this.formHandleStore.isChanged) {
                    this.formHandleStore.saveCallback()
                }
            }
        };
    }

    DynamicHeader = observer(() => {
        let breadcrumbs = [
            {
                label: 'Пользователи',
                path: this.props.userStore.hasPerm("system") ? ROUTES.users : undefined
            },
            {
                label: 'Редактировать пользователя',
            },
        ];

        let isFetching = [
            this.formHandleStore.isFetching,
            this.passwordFormStore.isFetching,
            this.groupsFormStore.isFetching,
            this.statusFormStore.isFetching
        ].some(el => el === true);

        return (
            <DynamicHeader
                breadcrumbs={breadcrumbs}
                buttonConf={this.buttonConf}
                isChanged={this.props.editStore.isChanged}
                isFetching={isFetching}
                label={this.formHandleStore.label}
            />
        )
    });

    render() {
        let {formStore} = this.formHandleStore;

        let tabs = [
            <Tab key={uuid4()}>{utils.getTranslation("Group.general")}</Tab>,
            <Tab key={uuid4()}>{"Изменить пароль"}</Tab>
        ];

        let tabPanels = [
            <TabPanel key={uuid4()}>
                {
                    this.formHandleStore.dataInitialized &&
                    <div className={this.formHandleStore.isFetching ? "disabled-form mt-5" : " mt-5"}>
                        <Form key={formStore.key}
                              store={formStore}
                              groupFiltering/>
                    </div>
                }
            </TabPanel>,
            <TabPanel key={uuid4()}>
                {
                    this.passwordFormStore.dataInitialized &&
                    <div className={this.passwordFormStore.isFetching ? "disabled-form mt-5" : " mt-5"}>
                        <Form key={this.passwordFormStore.formStore.key}
                              store={this.passwordFormStore.formStore}
                              groupFiltering/>
                    </div>
                }
            </TabPanel>
        ];

        if (this.groupsFormStore.dataInitialized) {
            tabs.push(<Tab key={uuid4()}>{"Группы"}</Tab>);
            tabPanels.push(
                <TabPanel key={uuid4()}>
                    {
                        this.groupsFormStore.dataInitialized &&
                        <div className={this.groupsFormStore.isFetching ? "disabled-form mt-5" : " mt-5"}>
                            <Form key={this.groupsFormStore.formStore.key}
                                  store={this.groupsFormStore.formStore}
                                  groupFiltering/>
                        </div>
                    }
                </TabPanel>
            )
        }

        if (this.statusFormStore.dataInitialized) {
            tabs.push(<Tab key={uuid4()}>{"Статус"}</Tab>);
            tabPanels.push(
                <TabPanel key={uuid4()}>
                    {
                        this.statusFormStore.dataInitialized &&
                        <div className={this.statusFormStore.isFetching ? "disabled-form mt-5" : " mt-5"}>
                            <Form key={this.statusFormStore.formStore.key}
                                  store={this.statusFormStore.formStore}
                                  groupFiltering/>
                        </div>
                    }
                </TabPanel>
            )
        }

        return (
            <React.Fragment>
                <this.DynamicHeader/>
                <Tabs className="page-content-body pl-5 pr-5">
                    <TabList>
                        {tabs}
                    </TabList>
                    {tabPanels}
                </Tabs>
            </React.Fragment>
        )
    }
}