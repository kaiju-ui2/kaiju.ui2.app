import React from "react";
import Header from "@kaiju.ui2/components/src/Header/base";
import TableComponent from "@kaiju.ui2/components/src/Tables/TableComponent";
import {inject, observer, Provider} from "mobx-react";
import {TableStore} from "@kaiju.ui2/components/src/Tables/stores";
import ROUTES from "System/systemRoutesNames";
import WindowFormComponent, {WindowFormStore} from "@kaiju.ui2/forms/src/WindowForm";


@inject("routerStore")
@inject("userStore")
export default class UsersPage extends React.Component {

    constructor(props) {
        super(props);

        this.tableStore = new TableStore({
            prefix: "Users",
            request: {
                method: "users_gui.grid"
            },
            rowCallback: (rowStore) => {
                this.props.routerStore.router.navigate(ROUTES.userEdit, {idOrUsername: rowStore.id})
            }
        });

        this.windowCreateFormStore = new WindowFormStore({
            label: 'Создать пользователя',
            formStore: {
                key: utils.uuid4(),
                prefix: "UsersCreate",
                disableGroup: true,
                hasHeader: false,
                requests: {
                    load: {
                        method: "users_gui.create_model",
                        params: {}
                    }
                }
            },
            requests: {
                save: {
                    method: "users_gui.create_user",
                    params: {}
                }
            },
            successCallback: (data) => {
                this.tableStore.actions._fetchTable(1)
            }
        });

        this.buttonConf = {
            label: utils.getTranslation("Button.create"),
            icon: "icon-plus",
            onClick: () => {
                this.windowCreateFormStore.toggleWindow();
            }
        };
    }

    DynamicHeader = observer(() => {
        let breadcrumbs = [
            {
                label: 'Система'
            },
            {
                label: 'Пользователи',
            },
        ];

        return (
            <Header
                breadcrumbs={breadcrumbs}
                searchCallback={(query) => this.tableStore.actions.fetchByQuery(query)}
                buttonConf={this.props.userStore.hasPerm("system") ? this.buttonConf : undefined}
            />
        )
    });

    render() {
        return (
            <React.Fragment>
                <Provider tableStore={this.tableStore} windowFormStore={this.windowCreateFormStore}>
                    <WindowFormComponent/>
                    <this.DynamicHeader/>
                    <div id="table">
                        <TableComponent paginationClassName="pl-5 pr-5" className="pb-5 pl-5 pr-5" key={utils.uuid4()}/>
                    </div>
                </Provider>
            </React.Fragment>
        )
    }
}

