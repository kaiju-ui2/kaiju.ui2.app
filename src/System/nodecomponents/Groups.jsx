import React from "react";
import {inject} from "mobx-react";
import {routeNode, RouteView} from "react-mobx-router5";
import systemRoutesNames from "System/systemRoutesNames";

const routeNodeName = systemRoutesNames.groupsRoot;

@inject("routerStore")
class Groups extends React.Component {

    render() {
        const {route, routerStore} = this.props;

        return (
            <React.Fragment>
                <RouteView route={route} routes={routerStore.routesMap} routeNodeName={routeNodeName}/>
            </React.Fragment>
        )
    }
}


export default routeNode(routeNodeName)(Groups);
