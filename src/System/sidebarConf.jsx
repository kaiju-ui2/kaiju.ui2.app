import systemRoutesNames from "System/systemRoutesNames";

const loadSidebarConf = () => {
    return [
        {
            "label": "Система",
            "routes": [
                {
                    "name": systemRoutesNames.users,
                    "rootName": systemRoutesNames.usersRoot,
                    "label": "Пользователи",
                    "permission": "system"
                },
                {
                    "name": systemRoutesNames.groups,
                    "rootName": systemRoutesNames.groupsRoot,
                    "label": "Группы",
                    "permission": "system"
                },
            ]
        }
    ];
};

export default loadSidebarConf;