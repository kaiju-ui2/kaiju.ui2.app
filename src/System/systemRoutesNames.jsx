const systemRoutesNames = {
    root: "system",

    rpc: "/public/rpc",

    groupsRoot: "system.groups",
    groups: "system.groups.all",
    groupEdit: "system.groups.edit",

    usersRoot: "system.users",
    users: "system.users.all",
    userEdit: "system.users.edit",
};

export default systemRoutesNames;