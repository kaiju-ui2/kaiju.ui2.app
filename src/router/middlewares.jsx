import userStore from "stores/userStore";
import editStore from "stores/editStore";
import routerStore from "stores/routerStore";
/**
 *  Если это не страница логина - получить текущего юзера
 *  => если он есть - редирект на основную или redirect-страницу
 *  => если его нет - остаться на этой странице
 */
const userMiddleware = (router) => (toState, fromState, done) => {

    if (toState.name === "login" && userStore.isLoggedIn) {
        const redirect = fromState ? fromState : {name: routerStore.defaultRoute};
        return Promise.reject({redirect: redirect})
    }

    done();
};

/**
 *  Если это НЕ страница логина и юзер не залогинен
 *  => redirect на логин
 *
 *  Проверяет permissions проставленные в routes в defaultParams.permission = "str"
 */
const permissionsMiddleware = (router) => (toState, fromState, done) => {

    if (toState.name !== "login" && !userStore.isLoggedIn) {
        return Promise.reject({redirect: {name: 'login', params: {referer: toState}}})
    }

    let permission = toState.params.permission;

    if (!permission) {
         done();
        return
    }

    if (!userStore.hasPerm(permission)) {
        toState["hasError"] = true;
        toState["code"] = 403;
        done();
        return
    }

    done();
};

const isEditState = (state) => {
    if (!state) {
        return false;
    }
    return state.name.includes("edit")
};

/* Get last node of node.
/* settings.edit.attributes => settings.edit
 */
const getLastNode = (state) => {
    let nodes = Object.keys(state.meta.params);
    return nodes[nodes.length - 2]
};

/*  Finds if state route is sibling of another route
*  settings.edit.attributes, settings.edit.something => true
*/
const isSiblingRoutes = (fromState, toState) => {
    return getLastNode(fromState) === getLastNode(toState)
};

/* Middleware to invoke browser changes alert.
 */
const saveChangesMiddleware = (router) => (toState, fromState, done) => {

    if (!isEditState(fromState)) {
        done();
        return
    }

    // FROM edit* ----> TO other page
    if (editStore.isChanged && !isEditState(toState) && !isSiblingRoutes(fromState, toState)) {
        let OK = confirm(utils.getTranslation("Message.lost_changes"));

        if (OK) {
            editStore.reset();
            done();
            return
        }
        return;
    }

    editStore.reset();
    done();

};

export {permissionsMiddleware, userMiddleware, saveChangesMiddleware}