import {action, observable, toJS} from "mobx";
import axios from "axios";

class AuthStore {
    @observable isFetching = false;
    @observable error = undefined;

    @observable values = {
        username: '',
        password: '',
    };

    @action setUsername(login) {
        this.values.username = (login || "").trim();
    }

    @action setPassword(password) {
        this.values.password = (password || "").trim();
    }

    @action reset() {
        this.values.username = '';
        this.values.password = '';
    }

    @action isValid() {
        if (!this.values.username) {
            this.error = utils.getTranslation("Login.input_username")
        } else if (!this.values.password) {
            this.error = utils.getTranslation("Login.input_password")
        } else {
            return true
        }

        return false
    }

    @action login() {
        if (!this.isValid()) {
            return Promise.resolve()
        }

        this.isFetching = true;
        this.error = undefined;

        return axios.post('/public/rpc', {
            method: "auth.login",
            params: toJS(this.values)
        }).then((response) => {
            if (response.data.result) {
                return response.data.result
            }

            throw response
        }).catch((response) => {
            this.error = utils.getTranslation("Login.invalid_user");
            console.error("LoginError: ", response)
        }).finally(() => {
            this.isFetching = false
        });
    }
}

export default new AuthStore();
