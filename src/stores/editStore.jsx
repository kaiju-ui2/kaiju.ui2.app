import {computed, observable} from "mobx";

/* Store is used for change state monitoring middleware.
/* In this middleware it will be checked on isChanged true|false
/*  Usage of prefix:
/*  prefix is for handle changing in different places in page.
/* For example different tabs logic. EditStore.setChanged(true, "first_tab | second tab")
*/
class EditStore {
    @observable states = {};

    setChanged(isChanged, prefix = "__change") {
        this.states[prefix] = isChanged;
    }

    @computed
    get isChanged() {
        return Object.values(this.states).some(value => value === true);
    }

    reset() {
        this.states = {}
    }
}

const editStore = new EditStore();
export default editStore;